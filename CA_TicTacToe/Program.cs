﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/* Создаем поле 3 x 3
 * Заполнить поле "пустотой"
 * Вывод поля на экран
 * Ход кого-то из игроков
 * Проверка, пустая ли ячейка
 * Если попал - "Х" или "О", переход хода
 * Проверка на конец игры
 * Если игра не окончена - переход на Вывод полей на экран
 */

namespace CA_TicTacToe
{
    class Program
    {
        enum Cell
        {
            Empty,
            Cross,
            Zero
        }
        enum Player
        {
            User,
            Comp
        }
        static void Main(string[] args)
        {
            const int N = 3;

            Cell[,] Field = new Cell[N, N];
            int firstMove = -1;
            Player currentPlayer = Player.Comp;

            Console.WriteLine("Кто будет первым начинать игру? (0 - компьютер, 1 - игрок)");
            do
            {
                firstMove = int.Parse(Console.ReadLine());

            } while (firstMove != 0 && firstMove != 1);

            if(firstMove == 0)
            {
                currentPlayer = Player.Comp;
            }
            else if (firstMove == 1)
            {
                currentPlayer = Player.User;
            }


            int movesCount = N*N;

            Random rnd = new Random();

            bool playGame = true;

            int iCell, jCell;

            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    Field[i, j] = Cell.Empty;
                }
            }

            while (playGame == true)
            {
                Console.Clear();
                Console.WriteLine("Крестики - нолики: ");
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        switch (Field[i, j])
                        {
                            case Cell.Empty:
                                Console.Write(".");
                                break;
                            case Cell.Cross:
                                Console.Write("X");
                                break;
                            case Cell.Zero:
                                Console.Write("O");
                                break;
                        }
                    }
                    Console.WriteLine();
                }

                switch (currentPlayer)
                {
                    case Player.User:
                        Console.WriteLine("Ход игрока:");
                        do
                        {
                            Console.WriteLine("Введите i:");
                            iCell = int.Parse(Console.ReadLine());

                            Console.WriteLine("Введите j:");
                            jCell = int.Parse(Console.ReadLine());
                        } while (iCell < 0 || iCell > N - 1 || jCell < 0 || jCell > N - 1 ||
                        Field[iCell, jCell] == Cell.Cross ||
                        Field[iCell, jCell] == Cell.Zero);

                        if (Field[iCell, jCell] == Cell.Empty)
                        {
                            Field[iCell, jCell] = Cell.Cross;
                            movesCount--;
                            currentPlayer = Player.Comp;
                        }
                        break;
                    case Player.Comp:
                        Console.WriteLine("Ход компьютера (Press Enter):");
                        do
                        {
                            iCell = rnd.Next(0, N);
                            jCell = rnd.Next(0, N);
                        } while (Field[iCell, jCell] == Cell.Cross ||
                        Field[iCell, jCell] == Cell.Zero);

                        if(
                            (Field[0, 1] == Cell.Cross && Field[0, 2] == Cell.Cross && Field[0, 0] == Cell.Empty) ||
                            (Field[1, 0] == Cell.Cross && Field[2, 0] == Cell.Cross && Field[0, 0] == Cell.Empty) ||
                            (Field[1, 1] == Cell.Cross && Field[2, 2] == Cell.Cross && Field[0, 0] == Cell.Empty))
                        {
                            Field[0, 0] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        else if ((Field[0, 0] == Cell.Cross && Field[0, 2] == Cell.Cross && Field[0, 1] == Cell.Empty) ||
                            (Field[1, 1] == Cell.Cross && Field[2, 1] == Cell.Cross && Field[0, 1] == Cell.Empty))
                        {
                            Field[0, 1] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        else if ((Field[0, 0] == Cell.Cross && Field[0, 1] == Cell.Cross && Field[0, 2] == Cell.Empty) ||
                            (Field[2, 0] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[0, 2] == Cell.Empty) ||
                            (Field[1, 2] == Cell.Cross && Field[2, 2] == Cell.Cross && Field[0, 2] == Cell.Empty))
                        {
                            Field[0, 2] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        else if ((Field[1, 1] == Cell.Cross && Field[1, 2] == Cell.Cross && Field[1, 0] == Cell.Empty) ||
                            (Field[0, 0] == Cell.Cross && Field[2, 0] == Cell.Cross && Field[1, 0] == Cell.Empty))
                        {
                            Field[1, 0] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        else if ((Field[1, 0] == Cell.Cross && Field[1, 2] == Cell.Cross && Field[1, 1] == Cell.Empty) ||
                            (Field[0, 2] == Cell.Cross && Field[2, 0] == Cell.Cross && Field[1, 1] == Cell.Empty) ||
                            (Field[0, 0] == Cell.Cross && Field[2, 2] == Cell.Cross && Field[1, 1] == Cell.Empty))
                        {
                            Field[1, 1] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        else if ((Field[1, 0] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[1, 2] == Cell.Empty) ||
                            (Field[0, 2] == Cell.Cross && Field[2, 2] == Cell.Cross && Field[1, 2] == Cell.Empty))
                        {
                            Field[1, 2] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        else if ((Field[0, 0] == Cell.Cross && Field[1, 0] == Cell.Cross && Field[2, 0] == Cell.Empty) ||
                            (Field[0, 2] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[2, 0] == Cell.Empty) ||
                            (Field[2, 1] == Cell.Cross && Field[2, 2] == Cell.Cross && Field[2, 0] == Cell.Empty))
                        {
                            Field[2, 0] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        else if ((Field[2, 0] == Cell.Cross && Field[2, 2] == Cell.Cross && Field[2, 1] == Cell.Empty) ||
                            (Field[0, 1] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[2, 1] == Cell.Empty))
                        {
                            Field[2, 1] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        else if ((Field[2, 0] == Cell.Cross && Field[2, 1] == Cell.Cross && Field[2, 2] == Cell.Empty) ||
                            (Field[0, 2] == Cell.Cross && Field[1, 2] == Cell.Cross && Field[2, 2] == Cell.Empty) ||
                            (Field[0, 0] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[2, 2] == Cell.Empty))
                        {
                            Field[2, 2] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        else if (Field[iCell, jCell] == Cell.Empty)
                        {
                            Field[iCell, jCell] = Cell.Zero;
                            movesCount--;
                            currentPlayer = Player.User;
                        }
                        Console.ReadKey();
                        break;
                }

                if (movesCount == 0 ||
                    (Field[0, 0] == Cell.Zero && Field[0, 1] == Cell.Zero && Field[0, 2] == Cell.Zero) ||
                    (Field[1, 0] == Cell.Zero && Field[1, 1] == Cell.Zero && Field[1, 2] == Cell.Zero) ||
                    (Field[2, 0] == Cell.Zero && Field[2, 1] == Cell.Zero && Field[2, 2] == Cell.Zero) ||
                    (Field[0, 0] == Cell.Zero && Field[1, 0] == Cell.Zero && Field[2, 0] == Cell.Zero) ||
                    (Field[0, 1] == Cell.Zero && Field[1, 1] == Cell.Zero && Field[2, 1] == Cell.Zero) ||
                    (Field[0, 2] == Cell.Zero && Field[1, 2] == Cell.Zero && Field[2, 2] == Cell.Zero) ||
                    (Field[0, 0] == Cell.Zero && Field[1, 1] == Cell.Zero && Field[2, 2] == Cell.Zero) ||
                    (Field[0, 2] == Cell.Zero && Field[1, 1] == Cell.Zero && Field[2, 0] == Cell.Zero) ||
                    (Field[0, 0] == Cell.Cross && Field[0, 1] == Cell.Cross && Field[0, 2] == Cell.Cross) ||
                    (Field[1, 0] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[1, 2] == Cell.Cross) ||
                    (Field[2, 0] == Cell.Cross && Field[2, 1] == Cell.Cross && Field[2, 2] == Cell.Cross) ||
                    (Field[0, 0] == Cell.Cross && Field[1, 0] == Cell.Cross && Field[2, 0] == Cell.Cross) ||
                    (Field[0, 1] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[2, 1] == Cell.Cross) ||
                    (Field[0, 2] == Cell.Cross && Field[1, 2] == Cell.Cross && Field[2, 2] == Cell.Cross) ||
                    (Field[0, 0] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[2, 2] == Cell.Cross) ||
                    (Field[0, 2] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[2, 0] == Cell.Cross))
                {
                    playGame = false;
                }
            }

            Console.Clear();
            Console.WriteLine("Крестики - нолики: ");
            for (int i = 0; i < N; i++)
            {
                for (int j = 0; j < N; j++)
                {
                    switch (Field[i, j])
                    {
                        case Cell.Empty:
                            Console.Write(".");
                            break;
                        case Cell.Cross:
                            Console.Write("X");
                            break;
                        case Cell.Zero:
                            Console.Write("O");
                            break;
                    }
                }
                Console.WriteLine();
            }

            if ((Field[0, 0] == Cell.Zero && Field[0, 1] == Cell.Zero && Field[0, 2] == Cell.Zero) ||
                (Field[1, 0] == Cell.Zero && Field[1, 1] == Cell.Zero && Field[1, 2] == Cell.Zero) ||
                (Field[2, 0] == Cell.Zero && Field[2, 1] == Cell.Zero && Field[2, 2] == Cell.Zero) ||
                (Field[0, 0] == Cell.Zero && Field[1, 0] == Cell.Zero && Field[2, 0] == Cell.Zero) ||
                (Field[0, 1] == Cell.Zero && Field[1, 1] == Cell.Zero && Field[2, 1] == Cell.Zero) ||
                (Field[0, 2] == Cell.Zero && Field[1, 2] == Cell.Zero && Field[2, 2] == Cell.Zero) ||
                (Field[0, 0] == Cell.Zero && Field[1, 1] == Cell.Zero && Field[2, 2] == Cell.Zero) ||
                (Field[0, 2] == Cell.Zero && Field[1, 1] == Cell.Zero && Field[2, 0] == Cell.Zero))
            {
                Console.WriteLine("Победа компьютера.");
            }
            else if ((Field[0, 0] == Cell.Cross && Field[0, 1] == Cell.Cross && Field[0, 2] == Cell.Cross) ||
                    (Field[1, 0] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[1, 2] == Cell.Cross) ||
                    (Field[2, 0] == Cell.Cross && Field[2, 1] == Cell.Cross && Field[2, 2] == Cell.Cross) ||
                    (Field[0, 0] == Cell.Cross && Field[1, 0] == Cell.Cross && Field[2, 0] == Cell.Cross) ||
                    (Field[0, 1] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[2, 1] == Cell.Cross) ||
                    (Field[0, 2] == Cell.Cross && Field[1, 2] == Cell.Cross && Field[2, 2] == Cell.Cross) ||
                    (Field[0, 0] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[2, 2] == Cell.Cross) ||
                    (Field[0, 2] == Cell.Cross && Field[1, 1] == Cell.Cross && Field[2, 0] == Cell.Cross))
            {
                Console.WriteLine("Вы победили.");
            }
            else
            {
                Console.WriteLine("Ничья.");
            }

            Console.ReadKey();
        }
    }
}
